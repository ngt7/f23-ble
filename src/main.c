#include <nrfx_power.h> // NOTE: This is not a Zephyr library!!  It is a Nordic NRFX library.

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/sys/printk.h>
#include <zephyr/logging/log.h>
#include <zephyr/smf.h>
#include <stdio.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/pwm.h>
#include <math.h>
#include <limits.h>
#include <zephyr/drivers/sensor.h>
#include "pressure.h"
#include "ble.h"
LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

//DEFINITIONS
#define ERROR_NODE DT_ALIAS(error)
#define HEARTBEAT_NODE DT_ALIAS(heartbeat)

#define GATHER_NODE DT_ALIAS(button0)
#define BLE_NODE DT_ALIAS(button1)

#define HEARTBEAT_CYCLE_TIME_MS 500 //1 Hz
#define ERROR_CYCLE_TIME_MS 500 //1 Hz

#define ADC2_MAX_MV 50
#define ADC2_MIN_MV 5
#define FREQ1 100
#define FREQEVENT1 1
#define ADC3_MAX_MV 150
#define ADC3_MIN_MV 10
#define FREQ2 500
#define FREQEVENT2 2
#define SAMPLINGMULTIPLIER 10 //to get enough samples
//total 1000 samples for LED1, 5000 samples for LED2
#define POWER_VOLTAGE_MV 3600.0 //battery voltage max in mV

#define MEASUREMENT_DELAY_MS 1000  // delay between measurements
#define OVERSAMPLE 10  // number of samples to average together

//STATE MACHINE DEVELOPMENT
static const struct smf_state device_state_machine[];

enum device_states { init_s, idle_s, gather_info_s, error_s, ble_notification_s, vbus_s };

struct s_object {
        struct smf_ctx ctx;
		struct k_event collectDone;
} s_obj;

//STRUCTS
static const struct gpio_dt_spec error_led = GPIO_DT_SPEC_GET(ERROR_NODE, gpios);
static const struct gpio_dt_spec heartbeat_led = GPIO_DT_SPEC_GET(HEARTBEAT_NODE, gpios);

static const struct gpio_dt_spec gather_button = GPIO_DT_SPEC_GET(GATHER_NODE, gpios);
static const struct gpio_dt_spec ble_button = GPIO_DT_SPEC_GET(BLE_NODE, gpios);

static struct gpio_callback gather_button_data;
static struct gpio_callback ble_button_data;

//INTS + FLOATS
float battery_lvl;
float led1_brightness;
float led2_brightness;
bool usbregstatus;
static int32_t atm_pressure_kPa;
float data_collector[3]; // { LED 1 Brightness, LED 2 Brightness, Pressure (Pa) }

void heartbeat_toggle(struct k_timer *heartbeat_timer);
void error_toggle(struct k_timer *error_timer);
void error_stop(struct k_timer *error_timer);
void gather_button_callback();
void ble_button_callback();

void heartbeatHandler();
void errorStartHandler();
void errorStopHandler();
void gatherHandler();
void bleHandler();

//Sampling Work/Handlers/Defines
void sin100hzTimerHandler();
void sin100hzTimerWork();
void sin100hzStopperHandler();
void sin100hzStopperWork();
void sin500hzTimerHandler();
void sin500hzTimerWork();
void sin500hzStopperHandler();
void sin500hzStopperWork();
K_TIMER_DEFINE(sin100hzTimer, sin100hzTimerHandler, sin100hzStopperHandler);
K_WORK_DEFINE(sin100hzWork, sin100hzTimerWork);
K_WORK_DEFINE(sin100hzStopWork, sin100hzStopperWork);
K_TIMER_DEFINE(sin500hzTimer, sin500hzTimerHandler, sin500hzStopperHandler);
K_WORK_DEFINE(sin500hzWork, sin500hzTimerWork);
K_WORK_DEFINE(sin500hzStopWork, sin500hzStopperWork);

int freq1values[FREQ1*SAMPLINGMULTIPLIER];
int freq2values[FREQ2*SAMPLINGMULTIPLIER];

//ADC IMPLEMENTATION
#define ADC_DT_SPEC_GET_BY_ALIAS(adc_alias)                    \
{                                                            \
    .dev = DEVICE_DT_GET(DT_PARENT(DT_ALIAS(adc_alias))),      \
    .channel_id = DT_REG_ADDR(DT_ALIAS(adc_alias)),            \
    ADC_CHANNEL_CFG_FROM_DT_NODE(DT_ALIAS(adc_alias))          \
}; 

static const struct adc_dt_spec adc_vadc_battery = ADC_DT_SPEC_GET_BY_ALIAS(vadc0);
static const struct adc_dt_spec adc_vadc_100hzsin = ADC_DT_SPEC_GET_BY_ALIAS(vadc1);
static const struct adc_dt_spec adc_vadc_500hzsin = ADC_DT_SPEC_GET_BY_ALIAS(vadc2);

//PWM IMPLEMENTATION
typedef struct pwm_led{
    int *values;
    int pos;
    const struct pwm_dt_spec led_pwm;
} FreqStruct;

FreqStruct led1 = {
    .values = freq1values,
    .pos = 0,
    .led_pwm = PWM_DT_SPEC_GET(DT_ALIAS(pwm1))
};

FreqStruct led2 = {
    .values = freq2values,
    .pos = 0,
    .led_pwm = PWM_DT_SPEC_GET(DT_ALIAS(pwm2))
};
//using these structs in the sampling

//HEARTBEAT WORK
K_WORK_DEFINE(heartbeat_work, heartbeat_toggle);

//HEARTBEAT TIMER
K_TIMER_DEFINE(heartbeat_timer, heartbeatHandler, NULL);

void heartbeatHandler(){
	k_work_submit(&heartbeat_work);
}

void heartbeat_toggle(struct k_timer *heartbeat_timer){
	//LOG_INF("heartbeat toggling");
	gpio_pin_toggle_dt(&heartbeat_led);
}

//ERROR WORK
K_WORK_DEFINE(error_start_work, error_toggle);
K_WORK_DEFINE(error_stop_work, error_stop);

//ERROR TIMER
K_TIMER_DEFINE(error_timer, errorStartHandler, errorStopHandler);

void errorStartHandler(){
	k_work_submit(&error_start_work);
}

void error_toggle(struct k_timer *error_timer){
	LOG_INF("error toggling");
	gpio_pin_toggle_dt(&error_led);
}	

void errorStopHandler(){
	k_work_submit(&error_stop_work);
}

void error_stop(struct k_timer *error_timer){
	gpio_pin_set_dt(&error_led, 0);
}

// BUTTON 1: Gather Info
K_WORK_DEFINE(gather_button_work, gather_button_callback);

void gatherHandler(){
	k_work_submit(&gather_button_work);	
}

void gather_button_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins){
	LOG_INF("Gather Button Pressed");
	smf_set_state(SMF_CTX(&s_obj), &device_state_machine[gather_info_s]);
};

//Sampling Functions
int16_t buf_adc0; //battery
int16_t buf_adc1; //led 1
int16_t buf_adc2; //led 2
int32_t battery_val_mv;

struct adc_sequence sequence_0 = {
    .buffer = &buf_adc0,
    .buffer_size = sizeof(buf_adc0), // bytes
};

struct adc_sequence sequence_1 = {
    .buffer = &buf_adc1,
    .buffer_size = sizeof(buf_adc1), // bytes
};

struct adc_sequence sequence_2 = {
    .buffer = &buf_adc2,
    .buffer_size = sizeof(buf_adc2), // bytes
};

int32_t read_adc(const struct adc_dt_spec *adc_spec, int16_t *buffer) {
    struct adc_sequence sequence = {
        .buffer = buffer,
        .buffer_size = sizeof(*buffer),
    };
    adc_sequence_init_dt(adc_spec, &sequence);
    int err = adc_read(adc_spec->dev, &sequence);
    if (err < 0) {
        LOG_ERR("Could not read ADC (err = %d)", err);
        return err;
    }
    int32_t val_mv = *buffer;  // Assign the raw value from the buffer to val_mv
    adc_raw_to_millivolts_dt(adc_spec, &val_mv);  // Convert the raw value to millivolts
    return val_mv;  // Return the millivolt value
}

void sin100hzTimerHandler(){
        k_work_submit(&sin100hzWork);
}
void sin100hzTimerWork(){
		if (led1.pos >= FREQ1*SAMPLINGMULTIPLIER){
                k_timer_stop(&sin100hzTimer);
                
        }
        else {
                led1.values[led1.pos] = read_adc(&adc_vadc_100hzsin, &buf_adc1);
                led1.pos++;
        }
}
void sin100hzStopperHandler(){
        led1.pos = 0;
        k_work_submit(&sin100hzStopWork);
}
void sin100hzStopperWork(){
        int maxSum = 0;
        int minSum = 0;
        for (int i = 0; i < FREQ1; i++){
                int tempMax = -9999;
                int tempMin = 9999;
                for (int j = 0; j < SAMPLINGMULTIPLIER; j++){
                        int readValue = led1.values[SAMPLINGMULTIPLIER*i + j];
                        if (readValue > tempMax){
                                tempMax = readValue;
                        }
                        if (readValue < tempMin){
                                tempMin = readValue;
                        }
                }
                maxSum += tempMax;
                minSum += tempMin;
        }
        maxSum /= FREQ1;
        minSum /= FREQ1;
        data_collector[0] = 100*((maxSum - minSum) - ADC2_MIN_MV)/(ADC2_MAX_MV-ADC2_MIN_MV);
        if (data_collector[0] < 0){
                data_collector[0] = 0;
        }
        LOG_INF("Brightness for LED1 VALUE: %f", data_collector[0]); //brightness percent for LED1
		k_event_post(&s_obj.collectDone, FREQEVENT1); //notifies this sampling is done
}

void sin500hzTimerHandler(){
		k_work_submit(&sin500hzWork);
}
void sin500hzTimerWork(){
		if (led2.pos >= FREQ2*SAMPLINGMULTIPLIER){
                k_timer_stop(&sin500hzTimer);
        }
        else {
                led2.values[led2.pos] = read_adc(&adc_vadc_500hzsin, &buf_adc2);
                led2.pos++;
        }
}
void sin500hzStopperHandler(){
        led2.pos = 0;
        k_work_submit(&sin500hzStopWork);
}
void sin500hzStopperWork(){
        int maxSum = 0;
        int minSum = 0;
        for (int i = 0; i < FREQ2; i++){
                int tempMax = -9999;
                int tempMin = 9999;
                for (int j = 0; j < SAMPLINGMULTIPLIER; j++){
                        int readValue = led2.values[SAMPLINGMULTIPLIER*i + j];
                        if (readValue > tempMax){
                                tempMax = readValue;
                        }
                        if (readValue < tempMin){
                                tempMin = readValue;
                        }
                }
                maxSum += tempMax;
                minSum += tempMin;
        }
        maxSum /= FREQ2;
        minSum /= FREQ2;
       data_collector[1] = 100*((maxSum - minSum) - ADC3_MIN_MV)/(ADC3_MAX_MV-ADC3_MIN_MV);
        if (data_collector[1] < 0){
                data_collector[1] = 0;
        }
        LOG_INF("Brightness for LED2 VALUE: %f", data_collector[1]); //brightness % for LED2
		k_event_post(&s_obj.collectDone, FREQEVENT2); //notifies this sampling is done
}

// BUTTON 2: BLE
K_WORK_DEFINE(ble_button_work, ble_button_callback);

void bleHandler(){
	k_work_submit(&ble_button_work);	
}

void ble_button_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins){
	LOG_INF("BLE Button Pressed");
	smf_set_state(SMF_CTX(&s_obj), &device_state_machine[ble_notification_s]);
};

//PRESSURE SENSOR
// honeywell_mpr is a default device in the Zephyr ecosystem
const struct device *const pressure_in = DEVICE_DT_GET_ONE(honeywell_mpr); 
static int32_t atm_pressure_kPa;

//INIT STATE
static void init_entry(void *o){
    int ret;
	
	if (!gpio_is_ready_dt(&heartbeat_led)) {
		return ret;
	}

	if (!gpio_is_ready_dt(&error_led)) {
		return ret;
	}

	ret = gpio_pin_configure_dt(&heartbeat_led, GPIO_OUTPUT_INACTIVE);
	if (ret < 0) {
		return ret;
	}

	ret = gpio_pin_configure_dt(&error_led, GPIO_OUTPUT_INACTIVE);
	if (ret < 0) {
		return ret;
	}

	ret = gpio_pin_configure_dt(&gather_button, GPIO_INPUT);
	if (ret < 0) {
		return ret;
	}

    ret = gpio_pin_configure_dt(&ble_button, GPIO_INPUT);
	if (ret < 0) {
		return ret;
	}

	gpio_init_callback(&gather_button_data, gatherHandler, BIT(gather_button.pin));		
	
	gpio_add_callback(gather_button.port, &gather_button_data);	

    gpio_init_callback(&ble_button_data, bleHandler, BIT(ble_button.pin));		
	
	gpio_add_callback(ble_button.port, &ble_button_data);	

	//Check that the ADC interface is ready
	if (!device_is_ready(adc_vadc_battery.dev)) {
		LOG_ERR("ADC controller device(s) not ready");
	}

	if (!device_is_ready(adc_vadc_100hzsin.dev)) {
		LOG_ERR("ADC controller device(s) not ready");
	}

	if (!device_is_ready(adc_vadc_500hzsin.dev)) {
		LOG_ERR("ADC controller device(s) not ready");
	}

	//Configure the ADC channel
	int err;
	err = adc_channel_setup_dt(&adc_vadc_battery);
	if (err < 0) {
		LOG_ERR("Could not setup ADC channel (%d)", err);
	}

	err = adc_channel_setup_dt(&adc_vadc_100hzsin);
	if (err < 0) {
		LOG_ERR("Could not setup ADC channel (%d)", err);
	}

	err = adc_channel_setup_dt(&adc_vadc_500hzsin);
	if (err < 0) {
		LOG_ERR("Could not setup ADC channel (%d)", err);
	}

	// check that the PWM controller is ready
	if (!device_is_ready(led1.led_pwm.dev))  {
		LOG_ERR("PWM device %s is not ready.", led1.led_pwm.dev->name);
		return -1;
}

    if (!device_is_ready(led2.led_pwm.dev))  {
            LOG_ERR("PWM device %s is not ready.", led2.led_pwm.dev->name);
            return -1;
    }
	
	//check that pressure sensor is ready
	if (!device_is_ready(pressure_in)) {
			LOG_ERR("MPR pressure sensor %s is not ready", pressure_in->name);
			return;
		}
		else {
			LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
		}

	//create event for when both pwm samplings are done
	k_event_init(&s_obj.collectDone);

	//Initialize Bluetooth
    err = bluetooth_init(&bluetooth_callbacks, &remote_service_callbacks);
    if (err) {
        LOG_ERR("BT init failed (err = %d)", err);
    }

	//check that pressure sensor is plugged in
	int kpa;
    kpa = sensor_sample_fetch(pressure_in); //detect sensor presence
	kpa = sensor_sample_fetch(pressure_in);
	kpa = sensor_sample_fetch(pressure_in);
        if (kpa != 0) {
            LOG_ERR("MPR sensor_sample_fetch error: %d", kpa);
            //CHANGE TO ERRORS FOR FINAL 
            smf_set_state(SMF_CTX(&s_obj), &device_state_machine[error_s]);
        } else {
                atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0); //measure atm pressure if working
				LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
        }

}

static void init_run(void *o){
    smf_set_state(SMF_CTX(&s_obj), &device_state_machine[idle_s]);
}

static void init_exit(void *o){
    k_timer_start(&heartbeat_timer, K_MSEC(HEARTBEAT_CYCLE_TIME_MS), K_MSEC(HEARTBEAT_CYCLE_TIME_MS));
	LOG_INF("Init Exit - Heartbeat starts");

}

//IDLE STATE
static void idle_entry(void *o){
    LOG_INF("Idle Entry");
    int ret;
	ret = gpio_pin_interrupt_configure_dt(&gather_button, GPIO_INT_EDGE_TO_ACTIVE);
    ret = gpio_pin_interrupt_configure_dt(&ble_button, GPIO_INT_EDGE_TO_ACTIVE);
	pwm_set_pulse_dt(&led1.led_pwm, (led1.led_pwm.period*data_collector[0]/100));
    pwm_set_pulse_dt(&led2.led_pwm, (led2.led_pwm.period*data_collector[1]/100));
}

static void idle_run(void *o){
    usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
    if (usbregstatus) { // VBUS detected
    smf_set_state(SMF_CTX(&s_obj), &device_state_machine[vbus_s]);
    } 
}

static void idle_exit(void *o){
	int ret;
	ret = gpio_pin_interrupt_configure_dt(&gather_button, GPIO_INT_DISABLE);
	ret = gpio_pin_interrupt_configure_dt(&ble_button, GPIO_INT_DISABLE);
	pwm_set_pulse_dt(&led1.led_pwm, 0);
    pwm_set_pulse_dt(&led2.led_pwm, 0);
}

//GATHER STATE
static void gather_info_entry(void *o){
	LOG_INF("Gather Entry");
	//start sampling timers unitl all samples collected
	k_timer_start(&sin100hzTimer, K_NO_WAIT, K_USEC(1000000/FREQ1/SAMPLINGMULTIPLIER)); //1000 samples/sec, 1 sample every 1000us
    k_timer_start(&sin500hzTimer, K_NO_WAIT, K_USEC(1000000/FREQ2/SAMPLINGMULTIPLIER)); //5000 samples/sec, 1 sample every 200us

	battery_val_mv=read_adc(&adc_vadc_battery, &buf_adc1);
	battery_lvl = battery_val_mv/POWER_VOLTAGE_MV*100; //battery level as a percent
	LOG_INF("Battery Level (percent): %f", battery_lvl);
	data_collector[2]=read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
	LOG_INF("Pressure: %f", data_collector[2]);
}

static void gather_info_run(void *o){
	usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
    if (usbregstatus) { // VBUS detected
    smf_set_state(SMF_CTX(&s_obj), &device_state_machine[vbus_s]);
    } 
	k_event_wait(&s_obj.collectDone, FREQEVENT1 | FREQEVENT2, true, K_FOREVER); //check if these are done forever
	smf_set_state(SMF_CTX(&s_obj), &device_state_machine[idle_s]); //back to idle if complete
}

//ERROR STATE
static void error_entry(void *o){
	LOG_INF("Error Entry");
	gpio_pin_set_dt(&error_led, 1);
}

//BLE NOTIFICATION STATE
static void ble_notification_entry(void *o){
	int err;
	ble_data[0]=data_collector[0];
	ble_data[1]=data_collector[1];
	ble_data[2]=data_collector[2];
	//LOG_INF("READY TO SEND: LED1 Brightness: %d", ble_data[0]);
	//LOG_INF("READY TO SEND: LED2 Brightness: %d", ble_data[1]);
	//LOG_INF("READY TO SEND: Pressure: %d", ble_data[2]);

	//battery GATT
	err = bt_bas_set_battery_level((int)battery_lvl);
		if (err) {
			LOG_ERR("BAS set error (err = %d)", err);
		}

	//send BLE
	battery_lvl =  bt_bas_get_battery_level();
	err = send_data_notification(current_conn, &ble_data, 3);
    if (err) {
        LOG_ERR("Could not send BT notification (err: %d)", err);
    }
    else {
        LOG_INF("BT data transmitted.");
    }

	smf_set_state(SMF_CTX(&s_obj), &device_state_machine[idle_s]);
}

//VBUS STATE
static void vbus_entry(void *o){
	LOG_INF("VBUS Detected");
	k_timer_start(&error_timer, K_MSEC(ERROR_CYCLE_TIME_MS), K_MSEC(ERROR_CYCLE_TIME_MS));
}

static void vbus_run(void *o){
	usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
        if (!usbregstatus) {
				smf_set_state(SMF_CTX(&s_obj), &device_state_machine[idle_s]);
}
}

static void vbus_exit(void *o){
	k_timer_stop(&error_timer);
}

static const struct smf_state device_state_machine[] = {
    [init_s] = SMF_CREATE_STATE(init_entry, init_run, init_exit),
    [idle_s] = SMF_CREATE_STATE(idle_entry, idle_run, idle_exit),
    [gather_info_s] = SMF_CREATE_STATE(gather_info_entry, gather_info_run, NULL),
    [error_s] = SMF_CREATE_STATE(error_entry, NULL, NULL),
    [ble_notification_s] = SMF_CREATE_STATE(ble_notification_entry, NULL, NULL),
    [vbus_s] = SMF_CREATE_STATE(vbus_entry, vbus_run, vbus_exit)
};


void main(void) {
	int ret;

	smf_set_initial(SMF_CTX(&s_obj), &device_state_machine[init_s]);
	while (1){
		ret = smf_run_state(SMF_CTX(&s_obj));
		if (ret) {
            break;
        }
	};
	return 0;
}