#include "pressure.h"

LOG_MODULE_REGISTER(pressure, LOG_LEVEL_INF);

int read_pressure_sensor(const struct device *pressure_in, int oversample, int atm_offset_kPa) {
    /*  Fetch-n-get pressure sensor data

        INPUTS:
            oversample (int) - number of serial data points to average together
            atm_offset_kPa (int) - atmospheric pressure offset (kPa); set to 0 for absolute pressure

        OUTPUTS:
            pressure_kPa (int)
            An error (-9999) will be returned if the pressure exceeds the range of the sensor.

    */

    int err;
    float pressure_kPa = 0;
    float raw_pressure_kPa = 0;
    int relative_pressure_kPa = 0;
    struct sensor_value pressure_vals = {.val1 = 0, .val2 = 0};

    LOG_DBG("Oversampling Factor: %d", oversample);

    for (int i=0; i < oversample; i++) {
        err = sensor_sample_fetch(pressure_in);
        if (err != 0) {
            LOG_ERR("MPR sensor_sample_fetch error: %d", err);
            return -9999;
        }
        else {
            err = sensor_channel_get(pressure_in, SENSOR_CHAN_PRESS, &pressure_vals);
            if (err != 0) {
                LOG_ERR("MPR sensor_channel_get error: %d", err);
                return -9999;
            }
        }
        
        // data returned in kPa
        raw_pressure_kPa = (float)pressure_vals.val1 + (float)pressure_vals.val2/1000000;
        LOG_DBG("Raw Pressure (kPa): %f", raw_pressure_kPa);
        pressure_kPa += raw_pressure_kPa;
    }

        pressure_kPa /= oversample;
        
        LOG_DBG("Mean Absolute Pressure (kPa): %f", pressure_kPa);

        relative_pressure_kPa = (int)pressure_kPa - atm_offset_kPa;
        LOG_INF("Mean Relative Pressure (kPa): %d", relative_pressure_kPa);

        return relative_pressure_kPa;
}
