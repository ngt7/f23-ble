# SPDX-License-Identifier: Apache-2.0

cmake_minimum_required(VERSION 3.20.0)
find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})
project(bme590-f23-ble)

target_sources(app PRIVATE src/main.c src/pressure.c src/ble.c)
